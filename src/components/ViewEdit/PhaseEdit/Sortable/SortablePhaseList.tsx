
import * as React from 'react';
import { SortableContainer } from 'react-sortable-hoc';

import SortableItem from './SortablePhaseItem';
import { PhaseItem } from '../Interfaces'

interface IProps {
  items: PhaseItem[],
  onPhaseEdit: any,
  onPhaseRemove: any,
}

const SortableList = SortableContainer( ({items, onPhaseEdit, onPhaseRemove} : IProps) => {
  return (
    <ul className='phases-form-inspacer'>
      {items.map( (item: PhaseItem, index: number) => (
        <SortableItem
          key={item.id}
          item={item}
          index={index}
          onEdit={onPhaseEdit}
          onRemove={onPhaseRemove} />
      ))}
    </ul>
  );
});

export default SortableList;