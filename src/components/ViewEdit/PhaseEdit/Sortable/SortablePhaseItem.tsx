// TODO: Further clean up this horrible mess.
// - Nested forms are invalid HTML
// - Move Formik and YUP validation up to main parent, move 
// - 
// - Improve total
// - Improve EUR value formatting

import * as React from 'react';
import * as EventMapper from './Eventmapper';
import { ChangeEvent } from 'react';
import {
  SortableElement,
  SortableHandle
} from 'react-sortable-hoc';
import { Button, Input, Row, Col, DatePicker, Icon, Modal } from 'antd';

import { PhaseItem } from '../../../ViewEdit/PhaseEdit/Interfaces';

import * as moment from 'moment';  // Moment is used by antd datepicker
import { Formik } from 'formik';
import * as Yup from 'yup';
import './SortablePhaseItem.css';
import { ETH_EUR_RATE } from '../constants';

const confirm = Modal.confirm;

const showConfirm = (id: string, onRemove: any) => {
  confirm({
    title: 'Confirm',
    content: 'Do you want to delete these items?',
    onOk() {
      onRemove(id);
    },
    onCancel() {
    },
  });
}

const DragHandle = SortableHandle(() =>
  <Icon type="menu" style={{ fontSize: '20px', color: '#08c' }} theme="outlined" />
);



interface SortableItemProps {
  item: PhaseItem,
  onEdit: any,
  onRemove: any,
}

const SortableItem = SortableElement(({ item, onEdit, onRemove }: SortableItemProps) => (
  <li>
    <Formik
      initialValues={{
        id: item.id,
        label: item.label,
        goal: item.goal,
        dueDate: item.dueDate,
      }}
      onSubmit={() => alert("This should never be called")}
      //   (values, { setSubmitting }) => {
      //   setTimeout(() => {
      //     alert(JSON.stringify(values, null, 2));
      //     setSubmitting(false);
      //   }, 500);
      // }

      validationSchema={Yup.object().shape({
        label: Yup.string()
          .required('Label Required'),
        goal: Yup.number()
          .required('Goal Required'),
        dueDate: Yup.date()
          .required('Required')
      })}
    >

      {props => {
        const {
          values,
          touched,
          errors,
          handleChange,
          handleBlur,
        } = props;
        return (
          <div >
            <Row className="sortable-item" type="flex" align="middle">

              <DragHandle />

              <Input
                id="label"
                placeholder="Phase Label (public)"
                type="text"
                value={values.label}
                onBlur={handleBlur}
                onChange={(event: ChangeEvent<HTMLInputElement>) =>
                  EventMapper.onChangeLabelEvent(event, item, onEdit, handleChange)
                }
                className={
                  !values.label || errors.label && touched.label ?
                    EventMapper.onErrorDetectEvent(item.id, true) : EventMapper.onErrorDetectEvent(item.id, false)
                }
              />

              <Input
                id="goal"
                placeholder="Enter your goal"
                type="number"
                min={0}
                value={values.goal}
                onChange={(event: ChangeEvent<HTMLInputElement>) =>
                  EventMapper.onChangeGoalEvent(event, item, onEdit, handleChange, !errors.goal)
                }
                className={
                  errors.goal ? 'error' : ''
                }
              />

              <p>ETH</p>

              <p className="eur-value">~ {Number(item.goal * ETH_EUR_RATE).toFixed(2)} EUR</p>

              <DatePicker
                className={!item.rowSuccess || !item.dueDate ? 'date-error' : ''}
                value={item.dueDate ? moment(item.dueDate, 'YYYY-MM-DD') : undefined}
                onChange={(date: moment.Moment, dateString: string) =>
                  EventMapper.onChangeDateEvent(date, dateString, item, onEdit)} />

              <div className="trash-placeholder">
                <Button
                  className="trash"
                  shape="circle"
                  icon="delete"
                  onClick={() =>
                    showConfirm(values.id, onRemove)
                  } />
              </div>
            </Row>
          </div>
        );
      }}
    </Formik>
  </li>
));

export default SortableItem;