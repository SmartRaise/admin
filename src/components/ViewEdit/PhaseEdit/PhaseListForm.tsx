// TODO: Clean up this mess. Properly use Formik
// - Move Formik into this level
// - Replace custom error handling (editPhasehandler) with Formik
// - Add Formik Field Array to add/remove elements and verfiy schema

import * as React from 'react';
import { arrayMove } from 'react-sortable-hoc';
import { Button, Row, Col, Alert } from 'antd';

import SortableList from './Sortable/SortablePhaseList';
import { generateRandomId } from '../DescriptionEdit/utils';
import { IPair, PhaseItem } from './Interfaces';
import { ETH_EUR_RATE } from './constants';
import './PhaseListForm.css'

interface IProps {
  onSubmit: Function
  initializePhases?;
}

interface PhaseListState {
  items: PhaseItem[],
  total: number,
  errorMessage: string,
  isFirst: boolean,
}

class PhaseListForm extends React.Component<IProps, PhaseListState> {
  state: PhaseListState = {
    // TODO: dummy items
    items: [
      { id: 'a1', label: '', goal: 0, dueDate: null, rowSuccess: true, formSuccess: true }
     ],
    total: 0,
    errorMessage: "",
    isFirst: true
  };

  constructor(props) {
    super(props)

    if (this.props.initializePhases) {
      this.state.items = this.props.initializePhases.map((phase, i) => {
        return {
          label: phase.label,
          goal: phase.goal,
          dueDate: phase.dueDate,

          id: i.toString,
          rowSuccess: true,
          formSuccess: true
        }
      })
    }
  }

  sortEndHandler = ({ oldIndex, newIndex }: IPair) => {
    this.setState({
      items: arrayMove(this.state.items, oldIndex, newIndex),
    });
    this.checkValidation();
  };

  editPhaseHandler = (id: string, type: string, value: any, hasError: boolean) => {
    let newItems = [...this.state.items];
    newItems.find((item, index): boolean => {
      if (item.id === id) {
        switch (type) {
          case 'goal':
            newItems[index].goal = Number(value);
            break;
          case 'dueDate':
            newItems[index].dueDate = value !== undefined ?
              new Date(value) : undefined;
            break;
          case 'label':
            newItems[index].label = value;
            break;
        }
        newItems[index].formSuccess = !hasError;
        return true;
      }
      return false;
    });
    this.setState({ items: newItems, total: this.getTotal() });
    this.checkValidation();
  }

  // TODO: Cleanup Why can't we use a normal map here?
  removePhaseHandler = (id: string) => {
    const newItems = this.state.items.filter(item => item.id !== id);
    this.setState({ items: newItems, total: this.getTotal() });
  }

  addPhaseHandler = () => {
    //TODO: Cleanup What's this, why do we need random Ids? Why are the ids string?
    const id = generateRandomId();
    let newItem = { id: id, label: "", goal: 0, dueDate: new Date(), rowSuccess: true, formSuccess: false };
    const newItems = [...this.state.items, newItem];
    this.setState({ items: newItems });
  }

  getTotal = (): number => {
    let total = 0;
    this.state.items.forEach(item => {
      total += item.goal;
    })
    return total;
  }

  clearRowSuccess = () => {
    let newItems = [...this.state.items];
    newItems.map((item, index) => {
      item.rowSuccess = true;
    });
    this.setState({ items: newItems });
  }

  // Returns true if dates are is ok. Needs refactoring.
  checkValidation = (): boolean => {

    // ???
    const itemCount = this.state.items.length;
    this.clearRowSuccess();
    let newItems = [...this.state.items];
    if (itemCount > 0 && this.state.items[0].dueDate) {
      newItems[0].rowSuccess = true;
      if (!this.state.items[0].rowSuccess) {
        this.setState({ items: newItems });
      }
    }

    // Checks that the dates are increasing; TODO: Cleanup - foreach
    var i: number;
    for (i = 0; i < itemCount - 1; i++) {
      let current = this.state.items[i];
      let next = this.state.items[i + 1];
      if (!current.dueDate || !next.dueDate) {
        break;
      }
      if (current.dueDate && next.dueDate && current.dueDate >= next.dueDate) {

        newItems[i + 1].rowSuccess = false;
        this.setState({ items: newItems });
        break;
      }
    }
    if (i < itemCount - 1) {
      this.setState({ errorMessage: "Date error: Each phase must have an end date and it must be later than the preceeding one." });
      return false;

    }
    return true;
  }

  submitHandler = () => {

    // Some kind of validation testing, tests the dates
    this.setState({ isFirst: false });
    if (!this.checkValidation()) {
      return;
    }

    // Check each item for errors
    const errorItems = this.state.items.filter((item, index) => item.formSuccess === false);
    if (errorItems.length > 0) {
      this.setState({ errorMessage: "Invalid Items: All fields must be filled in and valid." });
      return;
    }
    this.setState({ errorMessage: "" });

    // Callback
    this.props.onSubmit(this.state.items);

    // console.log("[PhaseList Submit] state items => ", this.state.items);
  }

  prepareAndSendTransaction

  componentDidMount() {
    this.setState({ total: this.getTotal() });
    this.checkValidation();
  }

  render() {
    return (
      <React.Fragment>
        <Row align='top'>
          <Col span={18}>
            <SortableList
              items={this.state.items}
              onSortEnd={this.sortEndHandler}
              onPhaseEdit={this.editPhaseHandler}
              onPhaseRemove={this.removePhaseHandler}
              useDragHandle />

            <form>
              <Row type="flex" justify="end">
                <Button className="button-addphase" type="default" onClick={this.addPhaseHandler}>
                  + Add Phase
                  </Button>
              </Row>
              <Row type="flex" justify="center">
                <div className="total-spacer" />
                <div className="total">Total Goal:
                {this.state.total} ETH
                ~ {Number(this.state.total * ETH_EUR_RATE).toFixed(2)} EUR
                </div>
              </Row>
              <Row>
                {
                  (this.state.errorMessage !== "" && !this.state.isFirst) &&
                  <Alert message={this.state.errorMessage} type="error" />
                }
              </Row>
              <Row>
                <Button className="button-submit" icon="cloud-upload" type="primary" onClick={this.submitHandler} >
                  Submit Phases
              </Button>
              </Row>
            </form>
          </Col>


          <Col span={6}>
            <Row align='top' >
              <div className="phase-desc">
                <p>- Phases have 30days grace period</p>
                <p>- Phases cannot be changed after launch</p>
              </div>
            </Row>
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export default PhaseListForm;