import { ChangeEvent } from "react";
import { PhaseItem } from "../Interfaces";
import { Moment } from "moment";

export const onChangeGoalEvent = (
    event: ChangeEvent<HTMLInputElement>,
    item: PhaseItem,
    onEdit: any,
    handleChange: any,
    hasError: boolean
  ) => {
    handleChange(event);
    onEdit(item.id, 'goal', Number(event.target.value) || 0, hasError);
  }
  
 export const onChangeDateEvent = (
    date: Moment,
    dateString: string,
    item: PhaseItem,
    onEdit: any,
  ) => {
    onEdit(item.id, 'dueDate', dateString || undefined, dateString === "")
  }
  
 export const onChangeLabelEvent = (
    event: ChangeEvent<HTMLInputElement>,
    item: PhaseItem,
    onEdit: any,
    handleChange: any,
  ) => {
    handleChange(event);
  
    onEdit(item.id, 'label', event.target.value, event.target.value === "");
  }
  
 export const onErrorDetectEvent = (id: string, hasError: boolean): string => {
    return hasError ? 'error' : '';
  }