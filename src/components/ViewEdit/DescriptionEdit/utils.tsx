export const generateRandomId = () => (
  (+ new Date() + Math.floor(Math.random() * 999999)).toString(36)
)

// TODO: revise func name
export async function getMeta(url: string) {   
  let img = new Image();
  await img.addEventListener("load", () => {
    return img.naturalWidth })
  img.src = url;
  return img.naturalWidth;
}

export const checkHttp = (url: string): boolean => {
  const url_ = url.toLowerCase();
  if(url_.startsWith("https:") || url_.startsWith("http:")) {
    return true;
  }
  return false;
}