import * as React from 'react'
import { ChangeEvent, Fragment } from 'react';
import {
  SortableElement,
  SortableHandle
} from 'react-sortable-hoc';
import { Button, Input, Icon, Modal } from 'antd';

// TODO: WTF ist das
import { JImage } from '../Interfaces'; 
import './SortableImageItem.css';

const confirm = Modal.confirm;

const showConfirm = (id: string, onRemove: any) => {
  confirm({
    title: 'Confirm',
    content: 'Do you want to remove these image?',
    onOk() {
      onRemove(id);
    },
    onCancel() {
    },
  });
}

interface IProps {
  item: JImage,
  removeImage: any
}

const SortableImageItem = SortableElement(({item, removeImage}: IProps) => {
  return (
    <div className="image-button">
      <img
        className="image-thumbnail"
        src={item.path} 
        alt={item.id} />
      <Button
        className="close-button"
        type="danger"
        size="small"
        onClick={() => showConfirm(item.id, removeImage)}>
        -
      </Button> 
    </div>
  );
});

export default SortableImageItem;