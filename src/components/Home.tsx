import * as React from "react";
import { Link } from "react-router-dom";

import 'antd/dist/antd.css';  // or 'antd/dist/antd.less'
// import "./assets/scss/Home.scss";

export type HomeProps = { }
export type HomeState = {}

class Home extends React.Component<HomeProps, HomeState> {

    constructor(props) { super(props); }

    render() {
        return (
            <div className="Home">
                <Link to="/new/">New</Link>
            </div>
        );
    }
}

export default Home;