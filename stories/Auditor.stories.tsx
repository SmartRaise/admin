import * as React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { AuditorMessage } from '../src/components/Auditor/AuditorMessage'


storiesOf('AuditorMessage', module)
  .addDecorator(story => <div className="admin-container-outer"><div className="admin-container-inner">{story()}</div></div>)
  .add('empty', () => <AuditorMessage />)
  .add('filled', () => <AuditorMessage initialValues={{ email: "email@example.com", auditorMessage: "This is my auditor message" }} />)


