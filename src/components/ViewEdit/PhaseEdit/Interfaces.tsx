
export interface IPair {
  oldIndex: number,
  newIndex: number
}

export interface PhaseItem  {
  id: string,
  label: string,
  goal: number,
  dueDate?: Date,
  rowSuccess: boolean,
  formSuccess: boolean
}