import * as React from  'react';
import { SortableContainer } from 'react-sortable-hoc';
import { Row } from 'antd';

import SortableImageItem from './SortableImageItem';
import {JImage} from '../Interfaces'

import './SortableImageList.css';


interface IProps {
  items: JImage[],
  onRemoveImage: any
}

const SortableImageList = SortableContainer( ({items, onRemoveImage} : IProps) => {
  return (
    <div className="image-list">
      {items.map( (item: JImage, index: number) => (
        <SortableImageItem
          key={item.id}
          item={item}
          index={index}
          removeImage={onRemoveImage} />
      ))}
    </div>
  );
});

export default SortableImageList;