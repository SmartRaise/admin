import * as React from "react";
import { ContractEdit } from "./ContractEdit";

import Web3 = require("web3");

import * as ProjectFullModel from "../../web3transactions/transactions/readProjectCore";

/**
 * This a Smart Component that hold state and manages the lifecycle of loading and storing data. 
 * The data loading/handling itself in the ProjectFullModell  
 */
type ContractFullLoadProps = {
    // We use key so it's auto-instantiated
    // https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html
    tM,
    contractAddress: string;
    web3: Web3;
};

type ContractFullLoadState = {
    contractValues?: null | ProjectFullModel.projectFullType;
    contractErrors?: null | string;
};

export class ContractFullLoad extends React.Component<ContractFullLoadProps, ContractFullLoadState> {
    timerId;

    constructor(props) {
        super(props);
        this.state = { contractValues: null }; // State keys are read-only after created
    }

    componentDidMount() {
        this._updateContractValues(); // We fire and don't wait around
    }

    /** This function loads the data from the chain, then it calls itself regularly */
    async _updateContractValues() {
        let contractResults = await ProjectFullModel.readProjectCore(
            this.props.web3,
            this.props.contractAddress
        );

        console.log("UpdateContactValues" + JSON.stringify(contractResults))
        this.setState({
            ...this.state,
            ...contractResults
        });

        // Update every 3 seconds. Disabled to avoid overwriting of CMS entries. TOOD: Proper touched/dirty management
        // this.timerId = setTimeout(() => this._updateContractValues(), 3000);
    }

    componentWillUnmount() {
        clearInterval(this.timerId);
    }

    render() {
        return (
            <ContractEdit
                tM={this.props.tM}
                contractAddress={this.props.contractAddress}
                contractErrors={this.state.contractErrors}
                contractValues={this.state.contractValues}
            />
        );
    }
}