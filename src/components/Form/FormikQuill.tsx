import React = require("react");
import ReactQuill from "react-quill";
import { FieldProps } from "formik";
import './form-basics.scss';

export interface FormikQuillProps {
    onChange;
    value: string;
}

export interface FormikQuillState {

}

class FormikQuill extends React.Component<FieldProps & FormikQuillProps, FormikQuillState> {
    constructor(props: FieldProps & FormikQuillProps) {
        super(props);
        // this.state = { :  };
    }

    quillModules = {
        toolbar: [
            ['bold', 'italic'],
            ['link'],
            ['clean']
        ],
    }

    quillFormats = [
        'header',
        'bold', 'italic', 'underline', 'strike',
        'link', 'image'
    ]

    render() {
        const wrapperStyle = this.props.form.errors[this.props.field.name] ? 'form-error-field' : 'form-field-wrapper'

        return (
            <div className={wrapperStyle}>
                <ReactQuill
                    onBlur={this.props.field.onBlur}
                    onChange={this.props.onChange}

                    value={this.props.value || ''} // 1)
                    modules={this.quillModules}
                    formats={this.quillFormats}
                />
            </div >
        );
    }
}

export default FormikQuill;


// 1) https://stackoverflow.com/questions/47318773/react-quill-error-you-most-probably-want-editor-getcontents-instead