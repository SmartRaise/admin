import * as React from "react";
import { Route } from "react-router-dom";
import DevTools from "mobx-react-devtools";

import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import "../assets/scss/App.scss";

import View from "./ViewEdit/View";
import New from "./New/New";
import Home from "./Home";
import { Web3Info } from "./HeaderFooter/Web3Info";
import { TopMenu } from "./HeaderFooter/TopMenu";
import Log from "./Log/Log";
import { TransactionManager } from "../web3transactions/transactionManager/TransactionManager";

export type AppProps = { web3; tM: TransactionManager };
export type AppState = {};

class App extends React.Component<AppProps, AppState> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="app">
        <DevTools />

        <TopMenu />

        <Route exact path="/" render={props => <Home />} />

        <Route
          path="/view/:address?"
          render={props => <View tM={this.props.tM} web3={this.props.web3} {...props} />}
        />

        <Route
          path="/new/"
          render={props => (
            <New web3={this.props.web3} tM={this.props.tM} {...props} />
          )}
        />

        <div className="bottomPane">
          <Web3Info web3={this.props.web3} />
          <Log TransactionJobs={this.props.tM.TransactionJobs} />
        </div>

      </div>
    );
  }
}

export default App;
