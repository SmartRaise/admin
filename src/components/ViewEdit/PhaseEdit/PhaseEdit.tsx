import React = require("react");
import PhaseListForm from "./PhaseListForm";
import { setPhases } from '../../../web3transactions/transactions/setPhases';
import { PhaseType } from '../../../web3transactions/transactions/readPhases';
import { PhaseItem } from './Interfaces'
import Web3 = require("web3");
import moment = require("moment");


export interface PhaseListProps {
    tM,                     // For writing to contract
    contractAddress,        // For writing to contract
    phases?: Array<PhaseType>   // Prepopulate fields 
}

export interface PhaseListState {

}

class PhaseList extends React.Component<PhaseListProps, PhaseListState> {

    async mapAndSend(newPhases) {

        // Map/transform to data that we need
        let setPhaseData = newPhases.map(
            (el) => {
                return {
                    label: el.label,
                    goal: el.goal,
                    dueDate: el.dueDate
                }
            })


        await setPhases(this.props.tM, this.props.contractAddress, setPhaseData)
    }

    mapContractToForm(contractPhases: Array<PhaseType>): Array<PhaseItem> {

        return contractPhases.map(
            (el, i) => {
                return {
                    id: i.toString(),
                    label: el.label,
                    goal: Number(Web3.utils.fromWei(el.goal.toString())),
                    dueDate: new Date (el.dueDate * 1000),
                    rowSuccess: true,
                    formSuccess: true
                }

            }
        )
    }

    constructor(props: PhaseListProps) {
        super(props);
        // this.state = { : };
    }
    render() {
        return (
            <PhaseListForm
                onSubmit={async (newPhases) => await this.mapAndSend(newPhases)}
                initializePhases={this.mapContractToForm(this.props.phases)} />

        )
    }
}


export default PhaseList;