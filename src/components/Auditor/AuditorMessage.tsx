import React = require("react");
import { Col, Row, Alert, Button } from "antd";
import { Formik, Form, Field, FormikProps, FieldProps } from "formik";
import '../Form/form-basics.scss';

import FormikQuill from "../Form/FormikQuill";
import * as Yup from 'yup';

export interface AuditorMessageProps {
    initialValues?: any     // We use this to pre-populate values when loading or testing
}

export interface AuditorMessageState {

}

export class AuditorMessage extends React.Component<AuditorMessageProps, AuditorMessageState> {
    constructor(props: AuditorMessageProps) {
        super(props);
        // this.state = { :  };
    }

    AuditorMessageSchema = Yup.object().shape({
        auditorMessage: Yup.string()
            .min(20, 'Too Short!')
            .max(500, 'Too Long!')
            .required('Required'),
    })

    render() {
        return (<>
            <h2> Auditor </h2>

            <Row align='top' type='flex'>
                <Col span={18}>
                    <Formik initialValues={this.props.initialValues}
                        onSubmit={values => alert(JSON.stringify(values, null, 2))}
                        validationSchema={this.AuditorMessageSchema}
                    >
                        {({ values, errors, touched, setFieldValue }) => (
                            <Form>
                                <Row className="custom-gap">
                                    <label>Auditor Message</label>
                                    <Field component={FormikQuill}
                                        name="auditorMessage"
                                        onChange={(content) => { setFieldValue('auditorMessage', content) }}
                                        value={values.auditorMessage}
                                        placeholder="First Name" />

                                    {Object.keys(errors).length != 0 &&
                                        <Alert
                                            message={JSON.stringify(errors)}
                                            type="error"
                                        />
                                    }
                                </Row>

                                <Row className="custom-gap">
                                    <Button type="primary" htmlType="submit" icon="cloud-upload">
                                        Submit Description
                                </Button>
                                </Row>

                            </Form>
                        )}
                    </Formik>

                </Col>
                <Col span={6}>
                    <Row align='top' >
                        <div className="phase-desc">
                            <p>- Phases have 30days grace period</p>
                            <p>- Phases cannot be changed after launch</p>
                        </div>
                    </Row>
                </Col>
            </Row>
        </>
        );
    }
}

export default AuditorMessage;