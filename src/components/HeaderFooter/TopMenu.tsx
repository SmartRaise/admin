import { Menu, Icon } from 'antd';
import * as React from 'react'
import { Link } from 'react-router-dom';

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

export class TopMenu extends React.Component {
  state = {
    current: 'mail',
  }

  handleClick = (e) => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  }

  render() {
    return (
      <Menu
        onClick={this.handleClick}
        selectedKeys={[this.state.current]}
        mode="horizontal"
      >
        <Menu.Item key="new">
          <Link to="/new/"><Icon type="file" />Create Campaign</Link>
        </Menu.Item>

        <Menu.Item key="edit">
          <Link to="/view/"><Icon type="edit" />Edit Campaign</Link>
        </Menu.Item>

        <Menu.Item key="audit">
          <Link to="#"><Icon type="audit" />Audit Campaign</Link>
        </Menu.Item>

      </Menu>
    );
  }
}