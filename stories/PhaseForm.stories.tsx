import * as React from 'react';
import BigNumber from "bignumber.js"

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import PhaseListForm from '../src/components/ViewEdit/PhaseEdit/PhaseListForm';
import PhaseList from '../src/components/ViewEdit/PhaseEdit/PhaseEdit';

import { PhaseItem } from '../src/components/ViewEdit/PhaseEdit/Interfaces'

// TODO: Goal needs to be transformed form wei after load
var testPhases: Array<Partial<PhaseItem>> =
  [{ label: "Phase1", goal: 100, dueDate: new Date("2019-02-20T00:00:00.000Z") }]


storiesOf('Phases', module)
  .addDecorator(story => <div className="admin-container-outer"><div className="admin-container-inner">{story()}</div></div>)
  .add('empty', () => <PhaseListForm onSubmit={console.log} />)
  .add('alert', () => <PhaseListForm onSubmit={alert} />)
  .add('initData', () => <PhaseListForm onSubmit={(e) => console.log('serarS!' + JSON.stringify(e))} initializePhases={testPhases} />)

  // .add('filled', () => <PhaseList initialValues={{ email: "email@example.com", auditorMessage: "This is my auditor message" }} />)