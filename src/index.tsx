import * as React from "react";
import { render } from "react-dom";
import { AppContainer } from "react-hot-loader";
import { HashRouter } from "react-router-dom";
import Web3 = require("web3");
declare global {
    interface Window {
        ethereum: any;
        // web3;
    }
}

import App from "./components/App";
import { TransactionManager } from "./web3transactions/transactionManager/TransactionManager";

const web3 = new Web3("ws://localhost:8545");
console.log("Web Version in index: " + web3.version)

const transactionManager = new TransactionManager(); // Processes and holds state about transactions

const rootEl = document.getElementById("root");

render(
    <AppContainer>
        <HashRouter>
            <App web3={web3} tM={transactionManager} />
        </HashRouter>
    </AppContainer>,
    rootEl
);

// Hot Module Replacement API
declare let module: { hot: any };

if (module.hot) {
    module.hot.accept("./components/App", () => {
        const NewApp = require("./components/App").default;

        render(
            <AppContainer>
                <HashRouter>
                    <App web3={web3} tM={transactionManager} />
                </HashRouter>
            </AppContainer>,
            rootEl
        );
    });
}
