import * as React from 'react'
import { Select, Button, Row, Col, Tag } from 'antd';
import { setCollecting as setCollecting, setHold } from '../../../web3transactions/transactions/setStatus'

const Option = Select.Option;

export const StatusEdit = ({ tM, contractAddress, projectStatus }) => {
    return (
        <Row>
            <Col span={12}>              <label>Project Status</label>

                <Tag color={projectStatusMap[projectStatus].color}>{projectStatusMap[projectStatus].label}</Tag>
            </Col>
            <Col span={6}>
                <Row type="flex" justify="end">
                    <Button
                        type="primary"
                        onClick={async () => await setCollecting(tM, contractAddress)}
                    >
                        Activate
                    </Button>
                    <Button
                        type="primary"
                        onClick={async () => await setHold(tM, contractAddress)}
                    >
                        Pause
                    </Button>
                </Row>
            </Col>
            <Col span={6}>
                {/* <div className="phase-desc">
                    <ul>
                        <li>- Once you set the status on Collecting, Phases are locked</li>
                    </ul>
                </div> */}
            </Col>
        </Row>
    )
}


function statusOptions() {
    return Object.keys(projectStatusMap).map((k) => {
        return (<Option value={k}>{projectStatusMap[k].label}</Option>)
    })
}

const projectStatusMap = {
    0: { color: "red", label: "New" },
    1: { color: "green", label: "Collecting" },
    3: { color: "yellow", label: "Paused" },
}
const fundingStatusMap = {
    0: { color: "red", label: "Payment Closed" },
    1: { color: "green", label: "Payment Collecting" },
}