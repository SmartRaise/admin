import * as React from 'react';
import BigNumber from "bignumber.js"
import Web3 = require("web3");


import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import PhaseListForm from '../src/components/ViewEdit/PhaseEdit/PhaseEdit';
import PhaseList from '../src/components/ViewEdit/PhaseEdit/PhaseEdit';

import { PhaseType } from '../src/web3transactions/transactions/readPhases';

// TODO: Goal needs to be transformed form wei after load
var testPhases: Array<PhaseType> =
  [{ label: "Phase1", goal: new BigNumber(Web3.utils.toWei('100')), dueDate: 1549117763, accumulatedGoal: new BigNumber(Web3.utils.toWei('100')) }]


storiesOf('PhaseList', module)
  .addDecorator(story => <div className="admin-container-outer"><div className="admin-container-inner">{story()}</div></div>)
  .add('initData', () => <PhaseList tM={null} contractAddress={null} phases={testPhases} />)

  // .add('filled', () => <PhaseList initialValues={{ email: "email@example.com", auditorMessage: "This is my auditor message" }} />)