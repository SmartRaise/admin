export type JImage = {
    id: string,
    path: string
}

export default JImage