import * as React from "react";

import { Alert, Input } from 'antd';
import 'antd/dist/antd.css';  // or 'antd/dist/antd.less'
// import "assets/scss/View.scss";
const Search = Input.Search;

import { ContractFullLoad } from "./ContractFullLoad";
import { verifySmartraiseOnAddress } from "../../web3transactions/common/verify";
import { RouteComponentProps } from "react-router-dom";


interface ClRouteMatch { address: string }

export type ClProps = { web3, tM }
export type ClState = { contractAddress, addressError }

/**
 * Shows a Load-Box and loads current contract into view
 * TODO: Split apart  
 */
class View extends React.Component<ClProps & RouteComponentProps<ClRouteMatch>, ClState> {

    constructor(props) {
        super(props)
        this.state = { contractAddress: null, addressError: null }
    }

    componentDidMount() {
        // We don't wait around
        this.verifyAndSetAddress(this.props.match.params.address)
    }

    async verifyAndSetAddress(contractAddress) {
        const addressVerification = await verifySmartraiseOnAddress(this.props.web3, contractAddress)

        if (addressVerification != true) {
            this.setState({ addressError: addressVerification })
        }
        else {
            this.setState({ contractAddress: contractAddress, addressError: null })
            this.props.history.push("/view/" + contractAddress);
        }
    }

    Jidenticon = () => {

        //@ts-ignore
        return <div className="contract-header-identicon" dangerouslySetInnerHTML={{ __html: jdenticon.toSvg(this.state.contractAddress, 70) }} />


    }

    render() {
        if (this.state.addressError != null) {
            var SearchError = <Alert message={this.state.addressError} type="error" showIcon />
        }

        // if (this.state.contractAddress != null) {
        //     //@ts-ignore
        //     var identicon = 
        // }

        return (
            <div className="View">
                {/* TODO: Split out Contract Search/Edit/Load */}
                {(this.state.contractAddress == null) ?
                    <>
                        <Search
                            placeholder={this.props.match.params.address}
                            onSearch={this.verifyAndSetAddress.bind(this)}
                            enterButton="Load Contract"
                        />
                        {SearchError}
                    </>
                    :
                    <div className="contract-header">
                        <this.Jidenticon />
                        <div className="contract-header-right">
                            <div className="contract-header-right__address">{this.state.contractAddress}</div>
                            {/* <div className="contract-header-right__title">Todo: Contract title</div> */}
                        </div>
                    </div>
                }

                {
                    (this.state.contractAddress != null) ?
                        <ContractFullLoad
                            tM={this.props.tM}
                            web3={this.props.web3}
                            key={this.state.contractAddress}
                            contractAddress={this.state.contractAddress}
                        />
                        : null
                }

            </div >
        );
    }
}

export default View;

