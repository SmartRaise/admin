import { Button, Col, Input, Modal, Row } from 'antd';
import * as React from 'react';
import { Component } from 'react';
import { arrayMove } from 'react-sortable-hoc';
import * as initialValue from '../../value0.json';
import { IPair } from '../PhaseEdit/Interfaces';
import { JImage } from './Interfaces'

import { checkHttp, generateRandomId, getMeta } from './utils';
import './ContentForm.css';
import SortableImageList from './sortable/SortableImageList';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import '../../Form/form-basics.scss'
import { debug } from 'util';
import { userInfo } from 'os';

const emptyValue = JSON.stringify(initialValue);
const Search = Input.Search;

interface ContentFormProps {
  onSubmit: Function,
  initialize: ContentFormState
}

export interface ContentFormState {
  items: JImage[],
  errorMessage: string,
  userinfo: string,
  description: string,
  title: string
}

class ContentForm extends Component<ContentFormProps, ContentFormState>{

  constructor(props) {
    super(props)

    this.state = {
      // TODO: dummy items
      items: props.initialize.items || [],
      errorMessage: props.initialize.errorMessage || "",
      userinfo: props.initialize.userinfo || "",
      description: props.initialize.description || "",
      title: props.initialize.title || ""
    };

    // { id: 'image1', path: 'https://images.unsplash.com/1/apple-gear-looking-pretty.jpg?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80' },
    // { id: 'image2', path: 'https://images.unsplash.com/1/type-away-numero-dos.jpg?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80' },
    // { id: 'image3', path: 'https://images.unsplash.com/1/iphone-4-closeup.jpg?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80' },
    // { id: 'image4', path: 'https://www.w3schools.com/howto/img_nature.jpg' },
    // { id: 'image5', path: 'https://www.w3schools.com/css/pineapple.jpg' }


  }
  sortEndHandler = ({ oldIndex, newIndex }: IPair) => {
    this.setState({
      items: arrayMove(this.state.items, oldIndex, newIndex),
    });
  };

  addImageHandler = async (newUrl: string) => {
    if (!checkHttp(newUrl)) {
      this.showErrorModal("Invalid Url for Http");
      return;
    }
    const newItems = [...this.state.items];
    try {
      const imageWidth = await getMeta(newUrl);
      if (imageWidth) {
        newItems.push({ id: generateRandomId(), path: newUrl });
        this.setState({ items: newItems });
      } else {
        this.showErrorModal("Invalid Url for Image");
      }
    } catch (error) {
      console.log("[Invalid image found]", error);
    }
  }

  removeImageHandler = (id: string) => {
    const newItems = this.state.items.filter(item => item.id !== id);
    this.setState({ items: newItems });
  }

  checkTextValidation = (): boolean => {
    return this.state.description !== emptyValue && this.state.userinfo !== emptyValue;
  }

  checkImageValidation = (): boolean => {
    return this.state.items.length > 0;
  }

  getValue = (name: string, value: string) => {
    switch (name) {
      case 'description':
        this.setState({ description: value });
        break;
      case 'userinfo':
        this.setState({ userinfo: value });
        break;
    }
  }

  showErrorModal = (message: string) => {
    Modal.error({
      title: 'Error',
      content: message,
    });
  }


  submitHandler = () => {
    const imageValid = this.checkImageValidation();
    const inputValid = this.checkTextValidation();
    if (imageValid && inputValid) {
      console.log('[Submit Images]');
      this.state.items.map(item => console.log(item.path));
      console.log('[Submit About Project]', this.state.description);
      console.log('[Submit About Us]', this.state.userinfo);
      this.setState({ errorMessage: "" });

      // Callback

      this.props.onSubmit(this.state.title, this.state.description, this.state.userinfo, this.state.items);


    } else {
      this.setState({ errorMessage: imageValid ? "Invalid input" : "No Images are selected." });
    }
  }


  quillModules = {
    toolbar: [
      ['bold', 'italic'],
      ['link'],
      ['clean']
    ],
  }

  quillFormats = [
    'header',
    'bold', 'italic', 'underline', 'strike',
    'link', 'image'
  ]

  render() {
    return (
      <div className="image-list-page">
        <h2>Description</h2>

        <Row gutter={16} type="flex">
          <Col span={18}>
            <Row className="custom-gap">
              <label>Project Headline</label>
              <Input placeholder=""
                // onChange={(value) => { this.setState({ title: value }) }}
                onChange={(e) => { console.log(e.target.value) }}
              />
            </Row>

            <Row className="custom-gap">
              <label>About the Project</label>
              <ReactQuill
                value={this.state.description}
                modules={this.quillModules}
                formats={this.quillFormats}
                onChange={(content) => { this.setState({ description: content }) }}
              />
            </Row>


            <Row className="custom-gap">
              <label>About Us</label>
              <ReactQuill
                value={this.state.userinfo}
                modules={this.quillModules}
                formats={this.quillFormats}
                onChange={(content) => { this.setState({ userinfo: content }) }}
              />
            </Row>

            <Row className="custom-gap">
              <label>Images</label>

              <SortableImageList
                axis="x"
                items={this.state.items}
                onSortEnd={this.sortEndHandler}
                onRemoveImage={this.removeImageHandler} />

            </Row>

            <Row className="custom-gap">

              <Search className="image-upload-input"
                placeholder="https://"
                enterButton="Add Image"
                onSearch={this.addImageHandler} />
            </Row>
            {
              this.state.errorMessage !== "" &&
              <Row
                className="custom-gap"
                type="flex"
                justify="end">
                <div className="validation-message">
                  {this.state.errorMessage}
                </div>
              </Row>
            }

            <Row className="custom-gap">
              <Button
                type="primary"
                onClick={this.submitHandler} >
                Submit Description
              </Button>
            </Row>
          </Col>

          <Col span={6}>
            <div className="phase-desc">
              <ul>
                <li>- Descriptions and images can be updated at any time. The old descriptions remain archived on the blockchain.</li>
                <li>- The first image is going to be the one shown on the overview</li>
              </ul>
            </div>
          </Col>
        </Row>

      </div >
    );
  }
}

export default ContentForm;