import * as React from "react";

import { Icon, Alert, Input, Button } from "antd";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import "../../assets/scss/New.scss";

import { deployCampaign } from "../../web3transactions/common/deploy";
import { RouteComponentProps } from "react-router-dom";

export type NewProps = {
    web3;
    tM;
};

export type NewState = {
    // contractAddress,
    // addressError
};

interface NewRouteMatch {
    // address: string
}

class New extends React.Component<
    NewProps & RouteComponentProps<NewRouteMatch>,
    NewState
    > {
    render() {
        return (
            <div>
                <h2>Create Empty Contract</h2>
                <Button
                    type="primary"
                    icon="cloud-upload"
                    onClick={async () => await deployCampaign(this.props.tM)}
                >
                    Deploy A New Contract
                </Button>
            </div>
        );
    }
}

export default New;
