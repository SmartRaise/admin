import * as React from "react";

import { Collapse } from "antd";
const Panel = Collapse.Panel;
var JSONPretty = require("react-json-pretty");

/// Using bluebird promises, they can be canceled and timeout
import * as BBPromise from "bluebird";
BBPromise.config({ cancellation: true });

type Web3InfoState = { networkStatus? };

export class Web3Info extends React.Component<{ web3 }, Web3InfoState> {
    constructor(props) {
        super(props);
        this.state = { networkStatus: "Loading..." };
    }

    componentDidMount() {
        this._checkNetwork();
    }

    _checkNetwork() {
        // We used to rebuild this every time because of caching issues in MetaMask. Needs recheck with latest releases
        // const web3 = new Web3("ws://localhost:8545");
        // TODO: Have global singleton/connection management for web3 with recovery

        const web3 = this.props.web3;

        const NetworkInfo = {
            currentProvider: web3.eth.currentProvider.constructor.name,
            peerCount: BBPromise.resolve(web3.eth.net.getPeerCount()),
            networkId: BBPromise.resolve(web3.eth.net.getId()),
            isListening: BBPromise.resolve(web3.eth.net.isListening()),
            blockNumber: BBPromise.resolve(web3.eth.getBlockNumber()),
            clientTime: Date.now(),
            latestBlock: web3.eth.getBlock("latest", true)
        };

        BBPromise.props(NetworkInfo)
            .timeout(3000)
            .then(res => {
                this.setState({ networkStatus: res });
            })
            .catch(res => {
                this.setState({
                    networkStatus: "Error occured: " + res.message
                });
            });

        setTimeout(this._checkNetwork.bind(this), 3100);
    }

    render() {
        // return (<p>{this.state.networkStatus}</p>);
        var headline;
        if (!this.state.networkStatus.currentProvider) {
            headline = this.state.networkStatus;
        } else {
            let ns = this.state.networkStatus;
            headline = `${ns.currentProvider} in network ${ns.networkId}, ${
                ns.peerCount
            } peers, latestBlock ${ns.blockNumber}`;
        }

        return (
            <Collapse>
                <Panel header={headline} key="1">
                    <JSONPretty
                        id="json-pretty"
                        json={this.state.networkStatus}
                    />
                </Panel>
            </Collapse>
        );
    }
}
// BBPromise.almost = r => BBPromise.all(r.map(p => p.catch ? p.catch(e => e) : p));
