import React = require("react");
import { observer } from "mobx-react";

import { Collapse } from "antd";
const Panel = Collapse.Panel;

import { TransactionJob } from "../../web3transactions/transactionManager/TransactionJob";

export interface LogProps {
    TransactionJobs: Array<TransactionJob>;
}

export interface LogState { }

/** This class observes the Transaction Jobs in Transaction Manager and displays their status */
@observer
class Log extends React.Component<LogProps, LogState> {
    constructor(props: LogProps) {
        super(props);
        // this.state = { :  };
    }

    /** TODO: Event processor for nicer display */
    renderLog = (log, i) => {
        var logLine = []
        logLine.push(<p> Log {i}: Status {log.status}, {log.label}, Hash: {log.hash|| "<none>"} </p>);

        if (log.receipt && log.receipt.contractAddress) { logLine.push(<p>Created contract: {log.receipt.contractAddress}</p>) }

        if (log.statusMessage) { logLine.push(<p>{log.statusMessage.message}</p>) }

        // log.receipt.events.RoleAdded[0].address
        // log.receipt.events.RoleAdded[0].returnValues.operator
        // log.receipt.events.RoleAdded[0].returnValues.role

        return logLine;
    };

    renderAllLogs = (logs: Array<TransactionJob>) =>
        logs.map((log, i) => {
            return <div key={i}>{this.renderLog(log, i)}</div>;
        });

    render() {
        const header =
            "Transaction logs (" + this.props.TransactionJobs.length + ")";

        return (
            <Collapse>
                <Panel header={header} key="1">
                    <div>{this.renderAllLogs(this.props.TransactionJobs)}</div>
                </Panel>
            </Collapse>
        );
    }
}

export default Log;
