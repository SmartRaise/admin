import React = require("react");
import { Alert, Tag, Tabs, Icon } from "antd";
import * as ProjectFullModel from '../../web3transactions/transactions/readProjectCore'
import { StatusEdit } from './StatusEdit/StatusEdit';
import PhaseList from './PhaseEdit/PhaseEdit';
import DescriptionEdit from './DescriptionEdit/DescriptionEdit';

const TabPane = Tabs.TabPane;

export interface ContractDisplayProps {
    tM,
    contractAddress,
    contractValues: ProjectFullModel.projectFullType,
    contractErrors
}
export interface ContractDisplayState { }

export class ContractEdit extends React.Component<ContractDisplayProps, ContractDisplayState> {
    constructor(props: ContractDisplayProps) {
        super(props);
    }

    render() {
        var cV = this.props.contractValues

        if (this.props.contractErrors) {
            var ContractError = <Alert message={this.props.contractErrors} type="error" showIcon />
        }

        if (this.props.contractValues == null) {
            var ContractInfo = <Alert message="Loading contract..." type="info" showIcon />
        }

        if (this.props.contractValues != null) {
            var contractReady = true;
        }
        // debugger

        return (<div>
            {/* <p>{JSON.stringify(this.props.contractValues)}</p> */}

            {ContractError}
            {ContractInfo}

            {(contractReady) &&
                <Tabs defaultActiveKey="1">
                    <TabPane tab={<span><Icon type="bars" />Basics</span>} key="1">
                        <h2>Status</h2>
                        <StatusEdit tM={this.props.tM} contractAddress={this.props.contractAddress} projectStatus={cV.statusProject} />
                  
                        <h2>Phases</h2>
                        <PhaseList tM={this.props.tM} contractAddress={this.props.contractAddress} phases={this.props.contractValues.phases}/>
                    </TabPane>
                    <TabPane tab={<span><Icon type="idcard" />Content</span>} key="2">
                        <DescriptionEdit tM={this.props.tM} contractAddress={this.props.contractAddress} descriptionObj={this.props.contractValues.description} />
                    </TabPane>
                </Tabs>
            }
        </div>);
    }
}

export default ContractEdit;