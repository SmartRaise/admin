import React = require("react");
import ContentForm from "./ContentForm";
import { setDescription } from '../../../web3transactions/transactions/setDescription';
import { DescriptionType } from '../../../web3transactions/transactions/readDescription';
import Web3 = require("web3");
import moment = require("moment");
import { debug } from "util";


export interface DescriptionEditProps {
    tM,                     // For writing to contract
    contractAddress,        // For writing to contract
    descriptionObj?   // Prepopulate fields 
}

export interface DescriptionEditState {

}

class DescriptionEdit extends React.Component<DescriptionEditProps, DescriptionEditState> {

    constructor(props: DescriptionEditProps) {
        super(props);
        // this.state = { : };
    }

    async mapAndSend(title, description, userinfo, imageItems) {


        const imageUrls = imageItems.map(el => { return el.path })

        const newDescription: DescriptionType = {
            title: title,
            aboutProject: description,
            aboutUs: userinfo,
            images: imageUrls
        }
        // Map/transform to data that we need

        await setDescription(this.props.tM, this.props.contractAddress, newDescription)
    }


    mapContractToForm(description: DescriptionType) {
        debugger
        var formImages;
        if (description.images) {
            formImages = description.images.map(
                (el, i) => {
                    return {
                        id: i.toString(),
                        path: el
                    }
                }
            )
        }

        const formDescription = {
            // TODO: dummy items
            title: description.title,
            items: formImages,
            errorMessage: "",
            userinfo: description.aboutUs,
            description: description.aboutProject
        };

        return formDescription;
    }

    render() {
        return (
            <ContentForm
                onSubmit={async (title, description, userinfo, imageItems) => await this.mapAndSend(title, description, userinfo, imageItems)}
                initialize={this.mapContractToForm(this.props.descriptionObj)}
            />

        )
    }
}


export default DescriptionEdit;